%include 'lib.inc'
global find_word
section .text

find_word:
	.loop:
		cmp rsi, 0
		je .fail
		mov r10, rsi
		mov r9, [rsi]
		add rsi, 8
		push r10
		call string_equals
		pop r10
		cmp rax, 1
		je .success
		mov rsi, r9
		jmp .loop
	.success:
		mov rax, r10
		ret
	.fail:
		xor  rax, rax
		ret
