global exit
global string_length
global print_string
global print_char
global print_err
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret ;length in rax

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length 
    mov  rdx, rax ; length
    mov  rsi, rdi ; string
    mov  rax, 1 ; to print
    mov  rdi, 1 ; where to print
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rsi, rsp
    mov  rdx, 1 ; bytes
    mov  rax, 1 ; write
    mov  rdi, 1 ; std out
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor r8, r8

    mov rax, rdi
    mov r10, 10

    .loop:
        xor rdx, rdx
        div r10
        push rdx
        inc r8
        cmp rax, 0
        jz .print
        jmp .loop
    .print:
        pop rdx 
        add rdx, 0x30
        mov rdi, rdx
        call print_char
        dec r8
        jnz .print
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax    
    cmp rdi, 0
    jge .print
    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    neg rdi
    .print:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    .loop:
        mov r10b, byte [rdi + r8]
        cmp byte [rsi + r8], r10b
        jne .fail
        cmp r10b, 0
        je .success
        inc r8
        jmp .loop
    .success
        mov rax, 1
        ret
    .fail
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rcx, rcx
    xor r10, r10

    .main_loop:
    	push rcx
	    push rdi
	    push rsi
	    call read_char
	    pop rsi
	    pop rdi
	    pop rcx
	    cmp rax, 0x20
	    je .check_space
	    cmp rax, 0x9
    	    je .check_space
	    cmp rax, 0xA
	    je .check_space
	    cmp rax, 0
	    je .success
        inc rcx
        cmp rcx, rsi
        je .overflow
        inc r10
        mov [rdi + rcx - 1], rax
        jmp .main_loop

    .check_space:
        cmp r10, 0
        je .main_loop
        mov [rdi + rcx], byte 0

    .success:
        mov rax, rdi
        mov rdx, rcx
        jmp .end

    .overflow:
	    xor rax, rax

    .end:
    	ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    mov r9, 10
    xor r10, r10

    .loop:
        mov r10b, byte [rdi + r8] 
        cmp r10b, 0x30
        jl .end
        cmp r10b, 0x39
        jg .end
        sub r10b, 0x30
        mul r9
        add rax, r10
        inc r8
        jmp .loop
    .end:
        mov rdx, r8
        ret
    
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r8, r8
    cmp byte [rdi], 0x2D
    je .parse_neg
    .parse_pos:
        call parse_uint
        ret 
    .parse_neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    call string_length
    cmp rax, rdx
    jge .fail
    
    xor r8, r8
    xor rcx, rcx
    .loop:
        mov r8b, byte [rdi + rcx]
        mov byte [rsi + rcx], r8b
        cmp r8b, 0
        je .success
        inc rcx
        jmp .loop

    .fail:
        xor rax, rax
        ret
    .success:
        ret

read_string:
    xor rax, rax
    xor rcx, rcx
    .main_loop:
	push rcx
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	pop rcx
	cmp rax, 0
	je .success
	cmp rax, 0xA
	je .success
	inc rcx
	cmp rcx, rsi
	je .overflow
	mov [rdi + rcx - 1], rax
	jmp .main_loop
     .success:
	mov [rdi + rcx], byte 0
	mov rax, rdi
	mov rdx, rcx
	jmp .end
     .overflow:
	xor rax, rax
     .end
	ret

print_err:
    xor rax, rax

    push rdi
    call string_length
    pop rdi

    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 2
    syscall
    
    call print_newline
	
    jmp exit
