%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%assign BUF_MAX_SIZE 255
extern find_word

section .bss
input: resb BUF_MAX_SIZE 

section .rodata
error: db 'Error', 0

section .text
global _start

_start:
	mov rdi, input
	mov rsi, BUF_MAX_SIZE
	call read_string
	cmp rax, 0
	je .error
	mov rdi, rax
	mov rsi, first_word
	call find_word
	cmp rax, 0
	je .error
	add rax, 8
	mov rdi, rax
	push rdi
	call string_length
	inc rax
	pop rdi
	add rdi, rax
	call print_string
	jmp .end
	.error:
		mov rdi, error
		call print_err
	.end:
		xor rdi, rdi
		call exit
